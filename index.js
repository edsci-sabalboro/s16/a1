const fName = document.getElementById("fName");
const lName = document.getElementById("lName");
const correctAnswers = {
      "question-1": "wolf",
      "question-2": "blew up"
    };

const submitButton = document.querySelector("#submit-button");
const totalScore = document.querySelector("#total-score");

submitButton.addEventListener("click", () => {
  let score = 0;
  
  for (const [questionId, answer] of Object.entries(correctAnswers)) {
    const selectedAnswer = document.querySelector(`#${questionId}`).value;
    const messageElement = document.querySelector(`#${questionId}-message`);

    if (selectedAnswer === answer) {
      messageElement.innerText = "Wow! You Got it Right!";
      messageElement.style.color = "blue";
      score++;
    } else {
      messageElement.innerText = "Try it Next Time!";
      messageElement.style.color = "red";
    }
  }

  totalScore.innerText = `Annyeong! ${fName.value} ${lName.value},You got ${score} out of ${Object.keys(correctAnswers).length}.`;

  alert(totalScore.innerText);
});

